/**------------------------------------------------------------------------------------------------
* @author  Hotboards Team
* @version v2.0.0
* @date    26-November-2017
* @brief   Just a simple wrapper to call the cmsis layer from a more suitable name.
-----------------------------------------------------------------------------------------------*/
#ifndef __cmsis_h__
#define __cmsis_h__

#ifdef __cplusplus
    extern "C" {
#endif

/*----include stm32f0 layer*/
#if defined (STM32F072xB)
    #include "cmsisf0.h"
#endif

#if defined (STM32F765xx) || defined (STM32F746xx)
    #include "cmsisf7.h"
#endif

#ifdef __cplusplus
    }
#endif

#endif

