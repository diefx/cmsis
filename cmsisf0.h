#ifndef __cmsisf0_h__
#define __cmsisf0_h__

#ifdef __cplusplus
    extern "C" {
#endif

/*----include stm32f0 layer*/
#include "stm32f0xx.h"

#define hal_gpio_t                         GPIO_TypeDef
#define hal_tim_t                          TIM_TypeDef

/*----Interrupts vector number assigned, read the micro header family*/
#define _hal_wwdg_irq                      WWDG_IRQn
#define _hal_rtc_irq                       RTC_IRQn
#define _hal_exti0_1_irq                   EXTI0_1_IRQn
#define _hal_exti2_3_irq                   EXTI2_3_IRQn
#define _hal_exti4_15_irq                  EXTI4_15_IRQn
#define _hal_usart2_irq                    USART2_IRQn
#define _hal_usb_irq                       USB_IRQn

/*----rutine for interrupt request redefinitions startup_stm32f0xxxx.h*/
#define hal_irq_nmi                         NMI_Handler
#define hal_irq_hardFault                   HardFault_Handler
#define hal_irq_sysTick                     SysTick_Handler
#define hal_irq_wwdg                        WWDG_IRQHandler
#define hal_irq_rtc                         RTC_IRQHandler
#define hal_irq_exti0_1                     EXTI0_1_IRQHandler
#define hal_irq_exti2_3                     EXTI2_3_IRQHandler
#define hal_irq_exti4_15                    EXTI4_15_IRQHandler
#define hal_irq_usart2                      USART2_IRQHandler
#define hal_irq_usb                         USB_IRQHandler


#ifdef __cplusplus
    }
#endif

#endif

