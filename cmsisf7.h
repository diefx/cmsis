#ifndef __cmsisf7_h__
#define __cmsisf7_h__

#ifdef __cplusplus
    extern "C" {
#endif

/*----include stm32f7 layer*/
#include "stm32f7xx.h"

#define hal_gpio_t                         GPIO_TypeDef
#define hal_tim_t                          TIM_TypeDef

/*----Interrupts vector number assigned, read the micro header family*/
#define _hal_wwdg_irq                      WWDG_IRQn
#define _hal_rtc_irq                       RTC_IRQn
#define _hal_usart2_irq                    USART2_IRQn
#define _hal_otg_fs_irq                    OTG_FS_IRQn


/*----rutine for interrupt request redefinitions startup_stm32f0xxxx.h*/
#define hal_irq_nmi                         NMI_Handler
#define hal_irq_hardFault                   HardFault_Handler
#define hal_irq_memManage                   MemManage_Handler
#define hal_irq_busFault                    BusFault_Handler
#define hal_irq_usageFault                  UsageFault_Handler
#define hal_irq_debugMon                    DebugMon_Handler
#define hal_irq_sysTick                     SysTick_Handler
#define hal_irq_wwdg                        WWDG_IRQHandler
#define hal_irq_rtc                         RTC_IRQHandler
#define hal_irq_usart2                      USART2_IRQHandler
#define hal_irq_otgFs                       OTG_FS_IRQHandler

/*----Macros*/
#define hal_scb_enableICache                SCB_EnableICache
#define hal_scb_enableDCache                SCB_EnableDCache


#ifdef __cplusplus
    }
#endif

#endif

